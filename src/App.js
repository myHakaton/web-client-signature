import './App.css';
import {Layout} from "antd";
import {PostHeader} from "./components/header";
import {PostContent} from "./components/content";

export const App = () => {
    return (
        <Layout className="App">
            <PostHeader/>
            <PostContent/>
        </Layout>
    );
};
