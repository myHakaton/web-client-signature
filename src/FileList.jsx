import React from "react";
import {Table} from "antd";
import moment from 'moment';
import {DownloadOutlined} from "@ant-design/icons";

const columns = [{
    title: 'Файл',
    dataIndex: 'name',
    width: '25%'
}, {
    title: 'Дата создания',
    dataIndex: 'lastModified',
    width: '15%',
    render: (val) => {
        return moment(val).format('DD.MM.YYYY HH:mm')
    }
}, {
    title: 'pdf',
    dataIndex: 'pdf',
    width: '25%',
    render: (val, {name}) => {
        if (!val) return null
        const nameFile = name.split('.')
        return (
            <a download={`${nameFile[0]}.pdf`}
               href={`data:application/pdf;base64,${val}`}
            >
                <DownloadOutlined/> {`${nameFile[0]}.pdf`}
            </a>
        )
    }
}, {
    title: 'sig',
    dataIndex: 'sig',
    width: '30%',
    render: (val, {name}) => {
        if (!val) return null
        const nameFile = name.split('.')
        return (
            <a download={`${nameFile[0]}.pdf.sig`}
               href={`data:application/octet-stream;base64,${val}`}
            >
                <DownloadOutlined/> {`${nameFile[0]}.pdf.sig`}
            </a>
        )
    }
}];

export const FileList = (props) => {
    return (
        <Table
            bordered
            rowKey={'uid'}
            columns={columns}
            dataSource={props.fileList}
            pagination={false}
            size={'small'}
        />
    )
}