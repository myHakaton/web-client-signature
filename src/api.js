import axios from 'axios';
import {message} from 'antd';

export const api = axios.create({
    timeout: 60000
});

export const axiosPost = async (url, request, config) => {
    try {
        const {data} = await api.post(url, request, {
            ...config
        });
        return data
    } catch (e) {
        console.error('axiosPost', e);
        message.error(`Ошибка запроса API (${url})`);
        throw e
    }
};

export const post = async (url, request, config) => {
    try {
        const response = await axiosPost(url, request, config);
        if (response.error && response.error.length > 0) {
            throw response.error[0]
        } else if (response.data) {
            return response.data
        } else {
            return response
        }
    } catch (e) {
        console.error('post', e);
        throw e
    }
};
