import {Layout} from "antd";
import './index.css';

export const PostHeader = () => {
    return (
        <Layout.Header className='post-header'>
            <div className='post-header-page'>
                <img className='post-logo'/>
                <div>
                    Модуль работы с ЭЦП для Почты России
                </div>
            </div>
        </Layout.Header>
    )
};