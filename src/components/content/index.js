import {Button, Divider, Layout, Typography, Upload} from "antd";
import {post} from "../../api";
import {DownloadOutlined, InboxOutlined, UploadOutlined} from "@ant-design/icons";
import {FileList} from "../../FileList";
import {useState} from "react";
import './index.css';
import {cerList} from "../../cerList";

export const PostContent = () => {
    const [sigInfo, setSigInfo] = useState(null);
    const [sigFileList, setSigFileList] = useState([]);
    const [fileList, setFileList] = useState([]);
    return (
        <Layout.Content className={'post-content'}>
            <div className={'post-content-page'}>
                <Typography.Title level={4}>
                    Тестовые сертификаты
                </Typography.Title>
                <Divider/>
                <div className={'action-sig-buttons'}>
                    {cerList.map((cer, index) => {
                        return (
                            <div>
                                <a className={'sig-a'}
                                   key={index}
                                   download={cer.name}
                                   href={`data:application/octet-stream;base64,${cer.base64}`}
                                >
                                    <DownloadOutlined/> {cer.name}
                                </a>
                            </div>
                        )
                    })}
                </div>
                <Typography.Title level={4}>Информация о сертификате</Typography.Title>
                <Divider orientation="right">
                    {sigFileList.length === 0 &&
                    <Upload accept={'.cer'}
                            fileList={sigFileList}
                            itemRender={() => null}
                            onRemove={(file) => {
                                const filterList = sigFileList.filter(item => item.uid !== file.uid);
                                setSigFileList(filterList);
                                setSigInfo(null);
                            }}
                            customRequest={async ({file, onSuccess}) => {
                                const formData = new FormData();
                                formData.append('file', file);
                                const sigInfo = await post('/api/save/cert', formData);
                                setSigFileList([...sigFileList, file]);
                                setSigInfo(sigInfo);
                            }}>
                        <Button icon={<UploadOutlined/>}>
                            Загрузить сертификат
                        </Button>
                    </Upload>
                    }
                    {sigFileList.length > 0 &&
                    <Button onClick={() => {
                        setSigFileList([]);
                        setSigInfo(null);
                    }}>
                        Удалить сертификат
                    </Button>
                    }
                </Divider>
                <p>{sigInfo}</p>
                <Typography.Title level={4}>Документы на подписание</Typography.Title>
                <Divider orientation="right">
                    <Upload accept={'.doc,.docx'}
                            fileList={fileList}
                            beforeUpload={(file) => {
                                setFileList([...fileList, file]);
                                return false
                            }}
                            onRemove={(file) => {
                                const filterList = fileList.filter(item => item.uid !== file.uid);
                                setFileList(filterList);
                            }}
                            itemRender={() => null}
                    >
                        <Button icon={<UploadOutlined/>}>
                            Загрузить документы
                        </Button>
                    </Upload>
                </Divider>
                <FileList fileList={fileList}/>
                <div style={{display: 'flex', justifyContent: 'center', margin: 10}}>
                    <Button disabled={fileList.length === 0 || !sigInfo}
                            type={'primary'}
                            onClick={async () => {
                                const formData = new FormData();
                                const newFileList = [...fileList]
                                newFileList.forEach(file => {
                                    formData.append('file', file);
                                });
                                const list = await post('/api/sign/sig', formData);
                                list.forEach((file, index) => {
                                    newFileList[index]['pdf'] = file.data;
                                    newFileList[index]['sig'] = file.sig;
                                });
                                setFileList(newFileList);
                            }}>
                        Подписать документы
                    </Button>
                </div>
            </div>
        </Layout.Content>
    )
}